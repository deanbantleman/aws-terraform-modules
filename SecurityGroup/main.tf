
# Create Windows security group
resource "aws_security_group" "Windows_Security_Group" {
  name = "${var.Win_SG_Name}"
 }

# Add security group rules
resource "aws_security_group_rule" "Allow_Inbound_RDP" {
  type = "ingress"
  from_port = "${var.Windows_sg_port_3389}"
  to_port = "${var.Windows_sg_port_3389}"
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.Windows_Security_Group.id}"
}

resource "aws_security_group_rule" "Allow_Inbound_WinRM" {
  type = "ingress"
  from_port = "${var.Windows_sg_port_winrm[0]}"
  to_port = "${var.Windows_sg_port_winrm[1]}"
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.Windows_Security_Group.id}"
}

resource "aws_security_group_rule" "Allow_Outbound_WinRM" {
  type = "egress"
  from_port = "${var.Windows_sg_port_winrm[0]}"
  to_port = "${var.Windows_sg_port_winrm[1]}"
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.Windows_Security_Group.id}"
}

resource "aws_security_group_rule" "Allow_Outbound_HTTP" {
  type = "egress"
  from_port = "${var.Sg_port_HTTP}"
  to_port = "${var.Sg_port_HTTP}"
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.Windows_Security_Group.id}"
}

resource "aws_security_group_rule" "Allow_Outbound_HTTPS" {
  type = "egress"
  from_port = "${var.Sg_port_HTTPS}"
  to_port = "${var.Sg_port_HTTPS}"
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.Windows_Security_Group.id}"
}

resource "aws_security_group_rule" "Allow_Outbound_DNS_TCP" {
  type = "egress"
  from_port = "${var.Sg_port_DNS}"
  to_port = "${var.Sg_port_DNS}"
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.Windows_Security_Group.id}"
}

resource "aws_security_group_rule" "Allow_Outbound_DNS_UDP" {
  type = "egress"
  from_port = "${var.Sg_port_DNS}"
  to_port = "${var.Sg_port_DNS}"
  protocol = "udp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.Windows_Security_Group.id}"
}

# output "Windows_Security_Group_ID" {
# value = "${aws_security_group.Windows_Security_Group.id}"
# }
